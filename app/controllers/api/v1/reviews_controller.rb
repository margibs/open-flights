module Api
  module V1
    class ReviewsController < ApplicationController
      protect_from_forgery with: :null_session

      def create
        review = airline.reviews.new(review_params)

        if review.save
          render json: ReviewSerializer.new(review).serialized_json
        else
          ender json: { error: review.errors.messages }, status: 422
        end
        
      end
      
      def destroy
        review = Review.find(params[:id])

        if review.destroy
          head :no_content
        else
          ender json: { error: review.errors.messages }, status: 422
        end
      end
      

      private

        def airline
          @airline ||= Airline.find(params[:airline_id])
        end
        
        def review_params
          params.require(:review).permit(:title, :description, :score)
        end
        
    end
  end
end